import json
from kafka import KafkaConsumer
from decouple import config
from time import sleep

class Consumer:
    def __init__(self, host, port, topics):
        self.consumer = KafkaConsumer(
            topics,
            bootstrap_servers=[f'{host}:{port}'],
            auto_offset_reset='earliest',
            # enable_auto_commit=True,
            # auto_commit_interval_ms=1000,
            max_poll_records=1,
            max_poll_interval_ms=2000,
            group_id=config('KAFKA_CONSUMER_GROUP'),
            value_deserializer=lambda msg: json.loads(msg.decode('utf-8')),
            key_deserializer=lambda key: key.decode('utf-8'))

    def get_consumer(self):
        return self.consumer

    def poll_one(self):
        # msg_pack = self.consumer.poll(timeout_ms=2000)
        # for tp, messages in msg_pack.items():
        #     for message in messages:
        #         # message value and key are raw bytes -- decode if necessary!
        #         # e.g., for unicode: `message.value.decode('utf-8')`
        #         print ("%s:%d:%d: key=%s value=%s" % (tp.topic, tp.partition,
        #                                             message.offset, message.key,
        #                                             message.value))
        #         return message

        sleep(1)
        for message in self.consumer:
            try:
                self.consumer.commit()
            except Exception:
                # self.logger.exception('Expception when commiting offsets')
                pass
            return message.value