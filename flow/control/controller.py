from blackboard.blackboard import Blackboard
from kafka_clients import consumer, producer

from decouple import config
from utils import utils
from log import log

import csv

class Controller:
    def __init__(self, host, port):
        self.logger = log.get_logger("CTRL")
        self.blackboard = Blackboard()

        # topics = [f'{service}.responses' for service in self.services]
        self.consumers = {}
        for service in ['wall', 'curls', 'hot-thing', 'insight']:
            self.consumers[service] = consumer.Consumer(host, port, f'{service}.responses')
        self.producer = producer.Producer(host, port)

    def investigate(self, anchor):
        # Checking if track exists in DB
        self.logger.info('Checking if track exists in DB')
        req = utils.get_config_json('wall/track_by_name.json')
        req['value']['object']['title'] = anchor['track']
        req['value']['object']['artist'] = anchor['artist']

        self.logger.debug(req)
        self.producer.publish(key=req['key'], 
            message=req['value'], 
            topic=req['topic'])
        
        self.logger.debug('Polling...')
        track = self.consumers['wall'].poll_one()
        self.logger.info(f'{track}')
        if not track:
            self.logger.info('Track does NOT exists in DB')
            self.logger.info('Searching for track in Spotify')
            req = utils.get_config_json('curls/search.json')
            req['value']['query'] = f"{anchor['artist']} {anchor['track']}"
            req['value']['type'] = 'track'

            self.producer.publish(key=req['key'], 
                message=req['value'], 
                topic=req['topic'])

            self.logger.info('Collecting search results from Kafka topic')
            message = self.consumers['curls'].poll_one()
            self.logger.debug(f'{message}')
            track_id = message['response']['_id']
            # if track_id:
            self.logger.info(f'Requesting track {track_id} from Spotify')
            req = utils.get_config_json('curls/track.json')
            req['value']['_id'] = track_id
            self.producer.publish(key=req['key'], 
                message=req['value'], 
                topic=req['topic'])

            self.logger.info('Collecting track from Kafka topic')
            response = self.consumers['curls'].poll_one()
            self.logger.debug(f'{response}')
            track_json = response['response']
            self.logger.debug(f'{track_json}')

            self.logger.info('Saving track in DB')
            req = utils.get_config_json('wall/track.json')
            req['value']['object'] = track_json

            self.producer.publish(key=req['key'], 
                message=req['value'], 
                topic=req['topic'])

            track=track_json

        self.logger.info('Initiating the track in the blackboard')
        self.blackboard.init_member('track', track)

        # sending message to models.
        self.logger.info("Requesting Insights")
        req = utils.get_config_json('insight/request.json')

        self.producer.publish(key=req['key'], 
            message=self.blackboard.track.__dict__, 
            topic=req['topic'])

        # Similar Songs
        self.logger.info('Requesting closest songs')
        req = utils.get_config_json('wall/similar_songs.json')
        req['value']['object']['_id'] = self.blackboard.track._id

        self.producer.publish(key=req['key'],
            message=req['value'],
            topic=req['topic'])
        similar_songs = self.consumers['wall'].poll_one()
        self.logger.debug(similar_songs)
        # self.logger.debug(similar_songs['tracks'][0].__class__)
        
        self.blackboard.init_member('similar_songs', similar_songs)

        # TODO same as above with recs model

        # Fill in artist/album info
        # self.logger.info('Filling in album and artists details')
        # self.fill_in_album_info()
        # self.fill_in_artists_info()

        # Collecting results from predictions
        self.logger.info('Collecting insights from kafka topic')
        insight_json = self.consumers['insight'].poll_one()
        self.logger.info('Initiating insights in blackboard')
        self.blackboard.init_member('insight', insight_json)

        # TODO Add recs model to flow

        self.logger.info('Publishing response')
        response = {
            'status': 200,
            'request': anchor,
            'response': self.blackboard.serialize()
        }

        self.logger.debug(response)
        self.producer.publish(key="api",
            message=response,
            topic="api.responses"
        )

        self.blackboard.clean()

    def fill_in_artists_info(self):
        artists_ids = []
        for artist in self.blackboard.track.artists:
            artists_ids.append(artist['_id'])

        self.logger.debug(f'Filling in all artists: {artists_ids}')
        artists = []
        # Checking if artist is in DB
        for _id in artists_ids:
            self.logger.debug(f'Filling in artist {_id}')
            db_query = {
                'type': 'artist',
                '_id': _id
            }
            self.producer.publish(key='GET', message=db_query, topic='wall.requests')
            artist = self.consumers['wall'].poll_one()
            if not artist:
                self.logger.debug(f'Artist {_id} is not in DB')
                self.logger.debug(f'Requesting artist {_id} from spotify')
                curls_query = {
                    '_id': _id
                }
                self.producer.publish(key='artist', message=curls_query, topic='curls.requests')
                artist = self.consumers['curls'].poll_one()

                self.logger.debug(f'Saving artist {_id} in DB')
                db_post = {
                    'type': 'artist',
                    'object': artist
                }
                self.producer.publish(key='POST', message=db_post, topic='wall.requests')

            artists.append(artist)

        self.blackboard.track.fill_artists(artists)

    def fill_in_album_info(self):
        _id = self.blackboard.track.album['_id']
        self.logger.debug(f'Filling in album {_id}')

        # Checking if album is in DB
        db_query = {
            'type': 'album',
            '_id': _id
        }
        self.producer.publish(key='GET', message=db_query, topic='wall.requests')
        album = self.consumers['wall'].poll_one()
        if not album:
            self.logger.debug(f'Album {_id} in not in DB')
            self.logger.debug(f'Requesting album {_id} from spotify')
            curls_query = {
                '_id': _id
            }
            self.producer.publish(key='album', message=curls_query, topic='curls.requests')
            album = self.consumers['curls'].poll_one()


            db_post = {
                'type': 'album',
                'object': album
            }
            self.producer.publish(key='POST', message=db_post, topic='wall.requests')

        self.blackboard.track.fill_album(album)

    def update_models(self, data):
        self.logger.info('Updating model')
        self.logger.info(f'Saving data to new CSV file named {data["date"]}')

        keys = data['new_chart'][0].keys()
        with open(f'{config("SHARED_VOLUME_PATH")}/{data["date"]}', 'w', newline='')  as output_file:   
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(data['new_chart'])