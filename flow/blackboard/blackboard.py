import classes.factory as factory 
import json

class Blackboard:
    def __init__(self):
        self.clean()

    # Serves both initialization and cleaning of blackboard
    def clean(self):
        # Defining members which are loaded dynamically
        self.track = None       # Anchor for the investigation
        self.insight = None    # Model-produced insights
        # self.recs = None        # Model-produced reccomendations
        self.similar_songs = None


    def init_member(self, member, _json):
        self.__dict__[member] = factory.create(_type=member, json=_json)

    def serialize(self):
        serialized_ss = None if self.similar_songs.__class__ == None.__class__ else self.similar_songs.serialize()
        serialized_value  ={
            'track' : self.track.serialize(),
            'insight' : self.insight.serialize(),
            'similar_songs' : serialized_ss
        } 
        return serialized_value