from classes.insight import Insight
from classes.track import Track
from classes.similar_songs import SimilarSongs
# from classes.recs import Recs

factory_dict = {
    'track': Track,
    'insight': Insight,
    'similar_songs': SimilarSongs
    # 'Recs': Recs.__init__
}

def create(_type, json):
    if _type in factory_dict and json:
        return factory_dict[_type](json)