import json
from classes.track import Track

class SimilarSongs:
    def __init__(self, tracks_json):
        self.tracks = []
        for track in tracks_json['tracks']:
            self.tracks.append(Track(track))

    def serialize(self):
        serialized_value = []
        for track in self.tracks:
            serialized_value.append(track.serialize())

        return serialized_value

    def __repr__(self):
        return str(self.serialize())