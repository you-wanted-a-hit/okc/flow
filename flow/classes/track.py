{
    '_id': '5QO79kh1waicV47BqGRL3g', 
    'title': 'Save Your Tears', 
    'lower_title': 'save your tears', 
    'duration_ms': 215626, 
    'preview_url': None, 
    'track_number': 11, 
    'album_id': '4yP0hdKOZPNshxUOjY0cZj', 
    'artists': [{
        '_id': '1Xyo4u8uXC1ZmMpatF05PJ', 
        'name': 'The Weeknd', 
        'lower_name': 'the weeknd', 
        'images': None, 
        'genres': None, 
        'followers': None, 
        'isPartial': True
    }], 
    'audio_features': {'danceability': 0.68, 'energy': 0.826, 'key': 0, 'loudness': -5.487, 'mode': 1, 'speechiness': 0.0309, 'acousticness': 0.0212, 'instrumentalness': 1.24e-05, 'liveness': 0.543, 'valence': 0.644, 'tempo': 118.051, 'duration_ms': 215627, 'time_signature': 4}, 
    'top_rank': 1000, 
    'num_of_weeks': 0, 
    'isPartial': False
}


import json

class Track:
    def __init__(self, info):
        #Extracting info
        self.title          = info['title']
        self._id            = info['_id']
        self.duration_ms    = info['duration_ms']
        self.preview_url    = info['preview_url']
        self.track_number   = info['track_number']

        self.album = info['album']
        if self.album.get('name'):
            self.album['title'] = self.album.pop('name')
        
        #Extracting artist
        self.artists = info['artists']

        if 'audio_features' in info:
            self.add_audio_features(info['audio_features'])

    def __repr__(self):
        return self.serialize()
    
    def add_audio_features(self,audio_features):
        #Extracting audio features
        self.audio_features = {
            'danceability'      : audio_features['danceability'],
            'energy'            : audio_features['energy'],
            'key'               : audio_features['key'],
            'loudness'          : audio_features['loudness'],
            'mode'              : audio_features['mode'],
            'speechiness'       : audio_features['speechiness'],
            'acousticness'      : audio_features['acousticness'],
            'instrumentalness'  : audio_features['instrumentalness'],
            'liveness'          : audio_features['liveness'],
            'valence'           : audio_features['valence'],
            'tempo'             : audio_features['tempo'],
            'duration_ms'       : audio_features['duration_ms'],
            'time_signature'    : audio_features['time_signature']
        }

    def fill_artists(self, artists):
        self.artists = artists

    def fill_album(self, album):
        self.album = album

    def serialize(self):
        return self.__dict__
