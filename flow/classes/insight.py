class Insight:
    def __init__(self, _json):
        self.clustering = {
            'cluster': _json['clustering']['cluster_pred'],
            'vector': _json['clustering']['cluster_vector'],
            'insights' : _json['clustering']['clustering_insights']
        }

        self.classification = {
            'genre':  _json['classification']['genre_pred'],
            'vector':  _json['classification']['genre_vector'],
            'insights': _json['classification']['classification_insights']
        }

        self.time_series = {
            'errors': _json['timeSeries']['errors'],
            'insight': _json['timeSeries']['timeSeries_insights'],
            'feature_vectors': _json['timeSeries']['feature_vectors']
        }

    def serialize(self):
        return self.__dict__