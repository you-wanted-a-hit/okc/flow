import os.path
import json

def validate_message(msg) -> bool:
    is_valid = False
    if msg.key == 'api':
        is_valid = 'track' in msg.value and 'artist' in msg.value
    elif msg.key == 'update':
        is_valid = 'new_chart' in msg.value and 'date' in msg.value

    return is_valid

def get_config_json(file_path):
    path = os.path.abspath(f'config/{file_path}')
    with open(path) as file:
        config_json = json.load(file)
        return config_json
