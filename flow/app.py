#External dependencies
from decouple import config

from control.controller import Controller
from kafka_clients.external_consumer import ExternalConsumer
from kafka_clients.producer import Producer
from utils import utils
from log import log

import threading
import queue

task_queue = queue.Queue()

def consume_daemon():
    logger = log.get_logger('C-DAEMON')
    
    logger.info('Setting up consumer')

    # Setting up consumer
    host = config('KAFKA_HOST_NAME')
    port = config('KAFKA_HOST_PORT')

    consumer_topic = config('KAFKA_CONSUMER_TOPIC')
    consumer = ExternalConsumer(host=host, port=port, topics=consumer_topic).consumer
    
    logger.info('Completed setup stage')

    for message in consumer:
        logger.info(f'New message received {message.key} -> {message.value}')
        task_queue.put(message)
        logger.info(f'New message is in queue, waiting to be proccessed')    
        try:
            consumer.commit()
        except Exception:
            logger.exception('Expception when commiting offsets')
   

def run():
    logger = log.get_logger('PROD-LOOP')

    # Setting producer instance
    logger.debug('Setting kafka producer')
    host = config('KAFKA_HOST_NAME')
    port = config('KAFKA_HOST_PORT')
    producer_topic = config('KAFKA_PRODUCER_TOPIC')
    producer = Producer(host=host, port=port)
    logger.debug('Setting kafka producer: DONE')

    # Launching consumer daemon
    threading.Thread(target=consume_daemon, daemon=True).start()
    controller = Controller(host, port)

    while True:
        logger.debug('Waiting for a new task')
        message = task_queue.get()
        if utils.validate_message(message):
            logger.info('Message is in valid format')
            logger.info('Requesting resources')
            controller.investigate(message.value)
        else:
            logger.debug('The sent message is not in a valid format')
            err_json = {
                'request'   : message.value,
                'error_msg' : 'The sent message is not in a valid format'
            }
            producer.publish("error", err_json, producer_topic)

    
