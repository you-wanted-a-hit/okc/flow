# State Machine Design
---
In this document, we will explain the design of our state machine, which leverages the Blackboard design pattern and makes use of Kafka as a messaging system.

## Table of contents
* [Motivation](#motivation)
* [Blackboard Design Pattern](#blackboard-design-pattern)
* [Automata](#automata)
* [Kafka Integration](#kafka-integration)
* [Implementation](#implementation)

## Motivation

Our project ― Data Science-powered insights into the music industry and song success ― is implemented over the increasingly popular microservices architecture, meaning modules are seperated into their own codebases. 

Each module is responsible for querying a different knowledge source, such as the Spotify API, or the Database. 

To investigate a song and provide insights regarding it's future success, we need to follow a flow (More on that in the [Automata](#automata) section) which includes querying the database, Third Party APIs, and the LSTM-based models, and then we need to combine all of the results into a readable and informative report for the end user.

That's where the Blackboard design pattern steps in.

## Blackboard Design Pattern

From Wikipedia:
>The blackboard pattern is a behavioral design pattern that provides a computational framework for the design and implementation of systems that integrate large and diverse specialized modules...

In human language, the blackboard pattern is a way to combine knowledge from different modules into a single structure. 

The pattern looks something like this diagram:

![Class UML](./structure.png)

Let's get into each part in detail, and specify how the microservices architecture 

### Blackboard

The blackboard is just like a real-life blackboard, where people can come and advertise information.

In the computer world, the "ads" are called Nodes, and each node is a specialized class matching the results supplied by its matching knowledge source.

So in short, the blackboard is a specialized data structure that defines the solution space.

### Control

The control is the "brains" of the design pattern. 

It signals the knowledge source to execute and find a solution to the problem the control supplies; for example, the control would signal the database driver to query for a specific song.

### Knowledge Sources

These are the modules discussed above. 

Each knowledge source is a specialized module that "solves a problem" in a different problem space.

## Automata

## Kafka Integration

## Implementation